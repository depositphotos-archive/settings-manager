package com.dph.settings.model.index;

import java.util.HashMap;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class Index extends HashMap<String,Object> {
    public String getName(){
        return (String) get("name");
    }

    public String getType(){
        return (String) get("indexType");
    }
}
