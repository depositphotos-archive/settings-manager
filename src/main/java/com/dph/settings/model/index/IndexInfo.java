package com.dph.settings.model.index;

import com.google.common.base.Optional;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class IndexInfo extends HashMap<String,List<Index>> {
  public Optional<List<Index>> getInfoForVersion(String version){
        return Optional.fromNullable(get(version));
    }

}
