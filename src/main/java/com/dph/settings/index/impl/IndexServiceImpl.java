package com.dph.settings.index.impl;

import com.dph.settings.model.index.IndexInfo;

import com.dph.settings.index.validator.IndicesValidator;
import com.dph.settings.index.*;

import javax.naming.ServiceUnavailableException;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class IndexServiceImpl implements IndexService{
    private IndexRequestor analyticUnitIndexRequestor;
    private IndexRequestor updaterIndexRequestor;
    private IndicesValidator indicesValidator;

    public void saveIndices(IndexInfo newIndices) throws ServiceUnavailableException {
     IndexInfo analyticIndices=analyticUnitIndexRequestor.getKnownIndices();
     indicesValidator.validateIndices(newIndices,analyticIndices);
     updaterIndexRequestor.saveIndices(newIndices);
    }

    public IndexInfo getIndices() throws ServiceUnavailableException {
      return updaterIndexRequestor.getKnownIndices();
    }

    public void setAnalyticUnitIndexRequestor(IndexRequestor analyticUnitIndexRequestor) {
        this.analyticUnitIndexRequestor = analyticUnitIndexRequestor;
    }

    public void setUpdaterIndexRequestor(IndexRequestor updaterIndexRequestor) {
        this.updaterIndexRequestor = updaterIndexRequestor;
    }

    public void setIndicesValidator(IndicesValidator indicesValidator) {
        this.indicesValidator = indicesValidator;
    }
}
