package com.dph.settings.index.impl;

import com.dph.settings.model.index.IndexInfo;
import com.dph.settings.index.*;
import org.springframework.web.client.RestTemplate;

import javax.naming.ServiceUnavailableException;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class AnalyticUnitIndexRequestor implements IndexRequestor {

    private RestTemplate restTemplate;
    private String analyticUnitURI;

    @Override
    public IndexInfo getKnownIndices() throws ServiceUnavailableException {
        try{
        return restTemplate.getForObject(analyticUnitURI,IndexInfo.class);
        }
        catch(Exception e){
                throw new ServiceUnavailableException();
        }
    }

    @Override
    public void saveIndices(IndexInfo newIndices) {
    throw new UnsupportedOperationException("analytic unit doesn't support updating index info");
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void setAnalyticUnitURI(String analyticUnitURI) {
        this.analyticUnitURI = analyticUnitURI;
    }
}
