package com.dph.settings.index.impl;

import com.dph.settings.model.index.IndexInfo;
import com.dph.settings.index.IndexRequestor;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.ServiceUnavailableException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class UpdaterIndexRequestor  implements IndexRequestor {
    private String indexSettingsPath;
    private ObjectMapper objectMapper;

    @Override
    public IndexInfo getKnownIndices() throws ServiceUnavailableException {
     try{
      return objectMapper.readValue(Paths.get(indexSettingsPath).toFile(), IndexInfo.class);
     }
     catch (Exception e){
         throw new ServiceUnavailableException();
     }
    }

    @Override
    public void saveIndices(IndexInfo newIndices) throws ServiceUnavailableException {
    try(FileOutputStream fileOutputStream=new FileOutputStream(Paths.get(indexSettingsPath).toFile())){
      OutputStreamWriter outputStreamWriter =new OutputStreamWriter(fileOutputStream);
      outputStreamWriter.write(new JSONObject(newIndices).toJSONString());
      outputStreamWriter.flush();
     }
     catch (Exception e){
       throw new ServiceUnavailableException();
     }
    }

    public void setIndexSettingsPath(String indexSettingsPath) {
        this.indexSettingsPath = indexSettingsPath;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
