package com.dph.settings.index;

import com.dph.settings.model.index.IndexInfo;

import javax.naming.ServiceUnavailableException;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public interface IndexService {
    public void saveIndices(IndexInfo indexInfo) throws ServiceUnavailableException;

    public IndexInfo getIndices() throws ServiceUnavailableException;
}
