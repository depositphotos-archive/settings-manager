package com.dph.settings.index;

import com.dph.settings.model.index.IndexInfo;

import javax.naming.ServiceUnavailableException;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public interface IndexRequestor {
    IndexInfo getKnownIndices() throws ServiceUnavailableException;
    void saveIndices( IndexInfo newIndices) throws ServiceUnavailableException;
}
