package com.dph.settings.index.validator.impl;

import com.dph.settings.index.validator.IndicesValidator;
import com.dph.settings.model.index.IndexInfo;
import com.dph.settings.index.validator.ValidationStrategy;

import java.util.List;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class IndicesValidatorImpl implements IndicesValidator{
    List<ValidationStrategy> validationStrategies;

    public void validateIndices(IndexInfo updaterIndices,IndexInfo analyticUnitIndices ){
        for (ValidationStrategy validationStrategy:validationStrategies){
            validationStrategy.validate(updaterIndices,analyticUnitIndices);
        }
    }

    public void setValidationStrategies(List<ValidationStrategy> validationStrategies) {
        this.validationStrategies = validationStrategies;
    }
}
