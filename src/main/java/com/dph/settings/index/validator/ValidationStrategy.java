package com.dph.settings.index.validator;

import com.dph.settings.model.index.IndexInfo;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public interface ValidationStrategy {

     void validate(IndexInfo updaterIndices,IndexInfo analyticUnitIndices);
}
