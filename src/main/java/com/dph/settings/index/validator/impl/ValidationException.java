package com.dph.settings.index.validator.impl;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class ValidationException extends RuntimeException {

    public ValidationException(String s) {
        super(s);
    }
}
