package com.dph.settings.index.validator.impl;

import com.dph.settings.index.validator.ValidationStrategy;
import com.dph.settings.model.index.Index;
import com.dph.settings.model.index.IndexInfo;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.springframework.cglib.core.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
public class CompatibilityValidationStrategy implements ValidationStrategy {
    private static final Function<Index, String> GET_INDEX_TYPES_FUNCTION=createGetIndexTypeFunction();
    @Override
    public void validate(IndexInfo updaterIndices, IndexInfo analyticUnitIndices) {
      for(Map.Entry<String,List<Index>> nextInfoForVersion:updaterIndices.entrySet()){
          Optional<List<Index>> analyticInfoForVersion=analyticUnitIndices.getInfoForVersion(nextInfoForVersion.getKey());
          if(!analyticInfoForVersion.isPresent()||!checkIndexTypesCompatibility(nextInfoForVersion.getValue(),analyticInfoForVersion.get())){
              throw  new ValidationException("new index isn't compatible with existing analytic unit: either versions aren't supported or some index types cannot be found in new settings");
          }
      }
    }
    private boolean checkIndexTypesCompatibility(List<Index> updaterIndexInfo, List<Index>analyticIndexInfo){
        Set<String> analyticIndexTypes=Sets.newHashSet(Collections2.transform(analyticIndexInfo,GET_INDEX_TYPES_FUNCTION));
        Set<String> updaterIndexTypes=Sets.newHashSet(Collections2.transform(updaterIndexInfo,GET_INDEX_TYPES_FUNCTION));
        return Sets.difference(analyticIndexTypes,updaterIndexTypes).isEmpty();
    }
    private static Function<Index, String> createGetIndexTypeFunction(){
        return new Function<Index, String>() {
            @Override
            public String apply(Index input) {
                return input.getType();
            }
        };
    }
}
