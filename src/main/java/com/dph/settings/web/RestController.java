package com.dph.settings.web;

import com.dph.settings.index.*;
import com.dph.settings.model.index.IndexInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.dph.settings.index.validator.impl.ValidationException;

import javax.naming.ServiceUnavailableException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by Iana Bondarska on 4/2/14.
 */
@Controller
public class RestController {
    private final Logger log = LoggerFactory.getLogger(RestController.class);

    @Autowired
    private IndexService indexService;

    @RequestMapping(value = "/indices", method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void saveIndices(@RequestBody IndexInfo bulkBody ) throws ServiceUnavailableException {
    indexService.saveIndices(bulkBody);
    }

    @RequestMapping(value = "/indices", method = GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<IndexInfo> getIndices() throws ServiceUnavailableException {
      return new ResponseEntity<IndexInfo>(indexService.getIndices(),HttpStatus.OK);
    }


    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ResponseEntity<String> handleValidationException(final ValidationException ex) {
        return new ResponseEntity<>(ex.getMessage(),HttpStatus.CONFLICT);
    }


    @ExceptionHandler(ServiceUnavailableException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ResponseBody
    public ResponseEntity<String> handleServiceUnavailableException(final ServiceUnavailableException ex) {
        return new ResponseEntity<>(ex.getMessage(),HttpStatus.SERVICE_UNAVAILABLE);
    }


    public void setIndexService(IndexService indexService) {
        this.indexService = indexService;
    }
}
