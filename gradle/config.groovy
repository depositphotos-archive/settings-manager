sonar {
    host {
        url = 'http://ci.depositphotos.net/sonar'
    }
    login = 'sonar'
    password = 'rp03amIn'

    jdbc {
        url = 'jdbc:mysql://ci.depositphotos.net/sonar'
        driverClassName = 'com.mysql.jdbc.Driver'
        username = 'sonar'
        password = 'rp03amIn'
    }
}

cargo {
    tomcat {
        containerId = 'tomcat7x'
        port = 8085
        host = 'valera.dev'
        user = 'tomcat-mg'
        password = 'oCX97hdgwTr--23'
    }
}

environments {

    dev {
        cargo {
            tomcat {
                host = 'postponed.depositphotos.com'
            }
        }
    }

    prod {
        cargo {
            tomcat {
                host = 'im.ss.depositphotos.com'
            }
        }
    }
}